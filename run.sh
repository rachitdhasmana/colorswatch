#!/bin/bash
app="color_swatch"
docker build -t ${app} .
docker run -d -p 5000:5000 \
  --name=${app} ${app}