The ColorSwatch App enables to genearte swatch of colors in different formats which are selected randomly.

To land on index page use URL: http://localhost:5000
To get the Color Swatch JSON use URL: http://localhost:5000/colorSwatch
this should generate exactly 5 random combination of color formats.
In case more samples are required use URL: http://localhost:5000/colorSwatch?colorcount=n (n being a positive integer)

Code Structure:
The Root directory for app is colorswatch.
All source code is hosted inside colorswatch/src.
Javascripts are hosted in folder colorswatch/src/static.
html templates are hosted in folder colorswatch/src/templates.

Following are the files that are used by APP:
colors.py: Backend Python file that maintains/generates color type specific data.
colorSwacthApp.py: Application file that sets up flask app and hosts APIs
static/showSwatch.js: JS file hosting jqury function to call colorSwatch API
templates/index.html: html template hosting the basic UI elements.

Library dependencies:
The App requires flask and numpy libraries to be installed along with python.

How to build the APP

There are three ways to build and run the app:

1. Command prompt: From the root directory run `pyhton colorSwacthApp.py`
This should start the server on `0.0.0.0:5000` and app can be accessed through browser as explained above.

2. Building Docker Image: There is a dockerfile added along that should help building an image with all dependencies.
From Root directory run: `bash run.sh` (this should build the docker image (color_swatch)
and run it in container color_swatch)
Once the script return successfully, the above mentioned URLs can be used to access the APP.

3. Docker Image from Docker Hub: The docker image of colorSwatch POC has been shared at below
URL:https://hub.docker.com/repository/docker/rachitdhasmana/color_swatch_repo

To run through the image directly follow the steps below:
    3a. Pull the docker image using cmd:  docker pull rachitdhasmana/color_swatch_repo:latest
        This should add an image to docker `rachitdhasmana/color_swatch_repo` (try docker image ls)
    3b. Run the docker image using cmd: docker run -d -p 5000:5000 --name=color_swatch rachitdhasmana/color_swatch_repo
        You should see the docker container running by name `color_swatch` (docker ps)

