FROM python:latest

RUN pip install flask
RUN pip install numpy
RUN mkdir /home/colorSwatchApp
RUN mkdir /home/colorSwatchApp/static
RUN mkdir /home/colorSwatchApp/templates

COPY  src/*.py /home/colorSwatchApp/
COPY  src/static/*.js /home/colorSwatchApp/static/
COPY  src/templates/*.html /home/colorSwatchApp/templates/

ENTRYPOINT ["python", "/home/colorSwatchApp/colorSwatchApp.py"]