from flask import Flask, jsonify, request, render_template
from numpy import random
from colors import AbstractColor

colorSwatchAPI = Flask(__name__, template_folder='templates')


@colorSwatchAPI.route('/colorSwatch', methods=['GET'])
def getColorSwatch():

    color_swatch = []
    color_types = AbstractColor.get_color_types()
    try:
        color_count_val = int(request.args.get('colorcount', 0))
    except ValueError:
        color_count_val = 0
    color_count = color_count_val if color_count_val > 0 else 5
    for color_count_index in range(color_count):
        index = random.randint(len(color_types))
        color_type_object = color_types[index]()
        color_swatch.append(color_type_object.get_color_object())
    return jsonify(color_swatch)

@colorSwatchAPI.route("/")
def getIndexPage():
    return render_template('index.html')

if __name__ == '__main__':
    colorSwatchAPI.run(host='0.0.0.0')
