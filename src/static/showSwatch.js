$(function() {
    $('#submit_btn').click(function() {
        $.ajax({
            url : '/colorSwatch?colorcount=' + $('#colorcount').val(),
            success: function(data) {
                var parentDiv = $("#colorSwatch");
                parentDiv.empty();
                for(var index=0; index < data.length; index++){
                    var newDiv = document.createElement('div');
                    newDiv.id = data[index].type + 'color' + index;
                    newDiv.className = 'swatchContainer';
                    newDiv.style = 'background-color:' + data[index].colorcode;
                    parentDiv.append(newDiv);
                }
            }
        });
    });
})