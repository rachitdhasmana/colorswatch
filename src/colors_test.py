import unittest
import colors as target


class TestAbstractColor(unittest.TestCase):

    def test_get_color_types_for_abstract_class(self):
        result = target.AbstractColor.get_color_types()
        self.assertEqual(len(result), 3)

    def test_get_color_types_for_hsl(self):
        result = target.HSLColor.get_color_types()
        self.assertEqual(len(result), 0)

    def test_get_color_types_for_rgb(self):
        result = target.RGBColor.get_color_types()
        self.assertEqual(len(result), 1)

    def test_get_color_types_for_brgb(self):
        result = target.BRGBColor.get_color_types()
        self.assertEqual(len(result), 0)

class TestHSLColor(unittest.TestCase):

    def test_get_css_color_code_given_values(self):
        color_obj = target.HSLColor(40, 30, 20)
        result = color_obj.get_css_color_code()

        self.assertTrue(result, 'hsl(40,30%,20%)')

    def test_get_css_color_code_random_values(self):
        color_obj = target.HSLColor()
        result = color_obj.get_css_color_code()

        self.assertTrue(result.startswith('hsl'))

    def test_get_color_object_given_values(self):
        color_obj = target.HSLColor(40, 30, 20)
        result = color_obj.get_color_object()

        self.assertTrue(result['type'], 'hsl')
        self.assertTrue(result['hue'], '40')
        self.assertTrue(result['saturation'], '30')
        self.assertTrue(result['lightness'], '20')
        self.assertTrue(result['colorcode'], 'hsl(40,30%,20%)')

    def test_get_color_object_random_values(self):
        color_obj = target.HSLColor()
        result = color_obj.get_color_object()

        self.assertTrue(result['type'], 'hsl')
        self.assertTrue(result['hue'], color_obj.hue_val)
        self.assertTrue(result['saturation'], color_obj.saturation_val)
        self.assertTrue(result['lightness'], color_obj.lightness_val)


class BaseRGBColor(object):
    target_klass = ''

    def test_get_css_color_code_given_values(self):
        color_obj = self.target_klass(40, 30, 20)
        result = color_obj.get_css_color_code()

        self.assertTrue(result, color_obj.color_type + '(40,30,20)')

    def test_get_css_color_code_random_values(self):
        color_obj = self.target_klass()
        result = color_obj.get_css_color_code()

        self.assertTrue(result.startswith(color_obj.color_type))

    def test_get_color_object_given_values(self):
        color_obj = self.target_klass(40, 30, 20)
        result = color_obj.get_color_object()

        self.assertTrue(result['type'], color_obj.color_type)
        self.assertTrue(result['red'], '40')
        self.assertTrue(result['green'], '30')
        self.assertTrue(result['blue'], '20')
        self.assertTrue(result['colorcode'], 'rgb(40,30,20)')

    def test_get_color_object_random_values(self):
        color_obj = self.target_klass()
        result = color_obj.get_color_object()

        self.assertTrue(result['type'], color_obj.color_type)
        self.assertTrue(result['red'], color_obj.red_val)
        self.assertTrue(result['green'], color_obj.green_val)
        self.assertTrue(result['blue'], color_obj.blue_val)


class TestRGBColor(BaseRGBColor, unittest.TestCase):
    target_klass = target.RGBColor


class TestBRGBColor(BaseRGBColor, unittest.TestCase):
    target_klass = target.BRGBColor


if __name__ == '__main__':
    unittest.main()
