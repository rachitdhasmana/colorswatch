from numpy import random

class AbstractColor(object):

    def get_color_object(self):
        raise NotImplemented('get_color_object needs implementation in specialised color class')

    def get_css_color_code(self):
        raise NotImplemented('get_css_color_code needs implementation in specialised color class')

    @classmethod
    def get_color_types(cls):
        color_types = []
        for subclass in cls.__subclasses__():
            color_types.append(subclass)
            color_types.extend(subclass.get_color_types())

        return color_types


class HSLColor(AbstractColor):

    color_type = 'hsl'
    def __init__(self, hue_val=None, saturation_val=None, lightness_val=None):
        self.hue_val = hue_val or random.randint(361)
        self.saturation_val = saturation_val or random.randint(101)
        self.lightness_val = lightness_val or random.randint(101)

    def get_css_color_code(self):
        color_code = self.color_type \
                    + '('\
                    + str(self.hue_val)\
                    + ','\
                    + str(self.saturation_val)\
                    + '%,'\
                    + str(self.lightness_val)\
                    + '%)'

        return color_code

    def get_color_object(self):
        return_value = {
            'type': self.color_type,
            'hue': self.hue_val,
            'saturation': self.saturation_val,
            'lightness': self.lightness_val,
            'colorcode': self.get_css_color_code()
        }

        return return_value

class RGBColor(AbstractColor):

    color_type = 'rgb'
    def __init__(self, red_val=None, green_val=None, blue_val=None):
        self.red_val = red_val or random.randint(256)
        self.green_val = green_val or random.randint(256)
        self.blue_val = blue_val or random.randint(256)

    def get_css_color_code(self):
        color_code = self.color_type \
                    + '('\
                    + str(self.red_val)\
                    + ','\
                    + str(self.green_val)\
                    + ','\
                    + str(self.blue_val)\
                    + ')'

        return color_code

    def get_color_object(self):
        return_value = {
            'type': self.color_type,
            'red': self.red_val,
            'green': self.green_val,
            'blue': self.blue_val,
            'colorcode': self.get_css_color_code()
        }

        return return_value

class BRGBColor(RGBColor):

    color_type = 'brgb'

    def __init__(self, red_val=None, green_val=None, blue_val=None):
        super(BRGBColor, self).__init__(
            red_val or random.randint(1001),
            green_val or random.randint(1001),
            blue_val or random.randint(1001))
